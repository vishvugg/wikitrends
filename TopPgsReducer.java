package org.amibee.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

/**
 * -Step 4.3-
 * 
 * The Reducer associated with the top pages job of WikiStats. This Reducer
 * extends the generic Reducer for the top pages job. This Reducer uses the
 * cleanup() method in order to cycle through all the top language codes,
 * printing each one with the associated number of unique pages, and then
 * printing the top pages and page spikes for that language.
 *
 */
public class TopPgsReducer extends TopPgsGenReducer<Text, LongWritable> {

	/**
	 * -Step 4.3.3-
	 * 
	 * Override the cleanup method in order to write out the data in the
	 * Reducer. This will be called once all records have been reduced. This
	 * method cycles through all the top language codes, printing each one with
	 * the associated number of unique pages, and then printing the top pages
	 * and page spikes for that language.
	 *
	 * @param context
	 *            The context associated with this job, can be used to access
	 *            the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {

		// Cycle through all the language codes
		for (Long uniquePages : mostPopularLanguages.descendingKeySet()) {

			String languageCode = mostPopularLanguages.get(uniquePages);

			// Cycle through all page spikes for the language code
			TreeMap<Long, ArrayList<String>> topPageSpikes = highestSpikePages
					.get(languageCode);
			for (Long pageSpike : topPageSpikes.descendingKeySet()) {

				ArrayList<String> pageNames = topPageSpikes.get(pageSpike);
				for (String pageName : pageNames) {

					// Write out the language code and page name key with the
					// page spike value
					context.write(new Text(languageCode + " " + pageName),
							new LongWritable(pageSpike));

				}
			}

			context.write(new Text("Language: " + languageCode),
					new LongWritable(uniquePages));
		}
	}
}
