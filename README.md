
  -Problem being solved-
 
  The problem addressed by this program is to identify the "N" Wikipedia pages
  that have experienced the largest activity increases (spikes) across any
  "O" day period for each of the "M" most common languages, where M, N and O
  are provided by the input data.
 
  As an example, if input values were given as M=5, N=10 and O=10, the program
  would output the ten Wikipedia pages that experienced the largest spike over
  any ten day period in the dataset for each of the five most common languages.
  The most common languages are those that have the largest number of unique pages
  over the entire dataset. Pages deleted during the time frame are still counted
  into this total.
 
  -Solution outline-

 Steps numbers are listed in the code as -step x.y- where they appear.
 
  1 Setup Configuration
    Read in command-line arguments. The arguments specify the input and output
    files. The arguments also specify the O day period to consider, the top M
    languages to be considered, and the N top spikes to consider for each of
    the M languages.
 
  2 Determine the top M languages.

    Determine the M languages that have the most unique pages over the input data.
 
    2.1 Mapping each language code.
        Read in each line of each input file. For each line, read the language
        code, ignoring codes that aren't two letters. Map each line as a composite of 
        language code and page name, mapped to null. 
 
    2.2 Reducing the language code occurrences.
        Receive each of the composites from 2.1. The important idea here is that these
        composites are sorted, first by language code, then by page name. For each page,
        read it in, if the language hasn't been seen, start a counter, if the language
        has been seen, increment the counter. When a new language is encountered, store the
        previous one in a mapping to the number of unique pages. On cleanup, flush the last language
        code, and then output each language code to the number of unique pages found.
        
    2.3 Mapping each page count.
    	 Read in each language code and page count from 2.3. Map out the language code
    	 mapped to the number of pages.
    
    2.4 Reducing the page counts.
    	 On setup, determine the number of top languages needed. For each language code, 
    	 a list of page counts is received. This list of page counts is summed in order to
    	 get the total number of unique pages. This language code is stored in a map along with
    	 the number of unique pages. On cleanup, the top M languages are retrieved from the 
    	 map and written out.
 
  3 Determine highest page spikes for each O day period.
 	 
    3.1 Mapping each page to page views.
 
        3.1.1 Setup list of top languages.
        	   Read in file for most popular language codes, read over file and store each
    	 	   code in a list of language codes. Match pattern of lines in the file with
    	 	   the pattern of <languageCode> <uniquePages>. If pattern is a match, add to
    	 	   list of most popular languages.
 
        3.1.2 Map pages for popular languages.
              Match input to list of most popular languages. If found in the
              list, cache as a successful find. If not found in list, cache as
              an unsuccessful find. This makes for more efficiency
              rather than looping every time.
        	   Write out page composite if it is in one of the top languages.
 
    3.2 Combining the page views.
        Cycle through all page views for each corresponding language code
        and page name. A spike is the difference between the lowest and highest
        page views for that page. Output the language code and page name
        mapped to the page spike.
 
    3.3 Reducing the page views.
    	 Cycle through all page views for each corresponding language code
        and page name. A spike is the difference between the lowest and highest
        page views for that page. Output the language code and page name
        and spike to be read in for the next phase.
 
  4 Determine highest page spikes for all periods.
 
    4.1 Mapping each page to the O day spike.
        Use output from reducer of all pages as input for Mapper of top pages.
        Input consists of language code, page name and spike for a specific
        "O day" period. Match input text to formatting of language, page name,
        spike, and write composite keys mapped to page spike.
 
    4.2 Combining the page spikes.
    	 See 4.3. The difference is that this will write out only page spikes, 
    	 not unique language pages.
 
    4.3 Reducing the page spikes.
        Iterate over list of page spikes for a given page to find the highest.
        Then loop through page spikes again to determine next highest and so
        on. If there are fewer than N pages, insert the page and spike in the
        list of top spikes for that page.
 
       4.3.1 Setup list of top languages and number of unique pages.
             Create map of top languages along with their number of
             unique pages. Populate this map by mapping each language code to
             the unique pages of that code.
 
       4.3.2 Map language code to set of top page spikes.
             Create hash map of pages with highest spikes for each language.
             Populate this map by scanning over all spikes of that language to
             find the highest. Then run over all spikes to find next highest.
             If there are fewer than N pages, insert these pages and spikes.
 
       4.3.3 Output top M languages, and each corresponding N page.
             Cycle through all language codes, then cycle through all page
             spikes for that language, writing out the language code and page
             name in descending order of page spike. Then write out the language 
             code and number of unique pages.
 
  -How to run the program-
 
 	Configuration

	To configure the tool, open "wikistats.pbs".
	Here, modify the number of nodes to use for this run. In addition, specify the max amount of time (wall time) to run for.
	Modify the below lines in "wikistats.pbs"
	#PBS -l nodes=NO_OF_NODES:ppn=8,pmem=2gb
	#PBS -l walltime=HH:MM:SS

	Next, edit the command line arguments section (lines 33-37). The arguments are:
	ARGSLANGS  	:	The number of popular languages to analyze the spikes for.
	ARGSSPIKES 	: 	The number of top page spikes to view for each of the popular languages.
	ARGSODAYS  	:	The O day period to use for this run. This will be every O day period over the input data. 
	ARGSNODES  	:	The number of nodes to use for this run. This is required as an argument in order to calculate the ideal number of reducers to use.
	ARGSMAXDAYS	:	The number of days to consider data over.

	Next, go to LOCALINPUT in line 50. Set this to the directory where the data is stored for input.
	Ideally, this directory will contain the same number of days as set in the max days args.
	Any additional files in the directory will be read over, but the data within will not be considered.

	Finally, go to LOCALOUTPUT in line 54. Set this to the directory where the output should be stored.
	The output will come in one file.

	For a collection of Wikistats data, see: /lustre/cs562178/wikistats
	The data in this directory is not guaranteed to be accurate or persistent.

	Execution

	To run the program, execute:
	chmod u+x runit.sh
	./runit.sh

	check progress using: qstat -U $USER

  -Sample output-
 
	The output for this tool will come in one file, with the following format:
	ln1 pageName1 pageSpike1
	...
	ln1 pageNameN pageSpikeN
	Language: ln1 uniquePages
	...
	lnM pageName1 pageSpike1
	...
	lnM pageNameN pageSpikeN
	Language: lnM uniquePages

	The pages with the highest spikes for a language are output first, followed by that language and the unique pages for the dataset.
	This is repeated for each of desired top languages.
	
	Sample output1:

	en		Main_Page	416922
	en		Main_Page	408376
	en		Main_Page	406799
	en		Main_Page	403604
	en		Main_Page	402964
	en		Main_Page	402073
	en		Main_Page	392830
	en		Main_Page	389642
	en		Main_Page	388404
	Language: en		24929013
	 
	Sample output2:
	
	en Main_Page	320359
	en Main_Page	286837
	en Special:Search	82324
	en Special:Random	78077
	en Special:Search	66666
	en Special:Random	61064
	en Dick_Clark	52143
	en Yakuza	44419
	en Jenny_McCarthy	43659
	en Petula_Clark	43233
	Language: en	31640207
	fr Pont_suspendu	23808
	fr Jacques_Martin_(animateur)	21760
	fr Poppy_Montgomery	19367
	fr Unforgettable	15537
	fr Sheila	15191
	fr Pont_suspendu	14727
	fr Sleepy_Hollow_(film)	12470
	fr Uranium	12296
	fr Windex.php	11291
	fr Le_Corniaud	10736
	Language: fr	5387861