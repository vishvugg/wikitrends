package org.amibee.split;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * A composite for WikiStats data consisting of a language code, page name,
 * and an id. The id is a reference to the "O day" period that a page
 * belongs to. For matters of comparing to other composites, the order of
 * comparing is id, language code, and page name. Each of the compared
 * fields are compared based on the natural ordering of the field.
 *
 */
public class LanguagePageIdComposite implements WritableComparable<LanguagePageIdComposite> {

	protected Text language;
	protected Text page;
	private IntWritable id;

	public LanguagePageIdComposite(Text language, Text page, IntWritable id) {
		this.language = language;
		this.page = page;
		this.id = id;
	}

	@Override
	public void readFields(DataInput input) throws IOException {
		language.clear();
		page.clear();
		language.readFields(input);
		page.readFields(input);
		id.readFields(input);
	}

	public LanguagePageIdComposite() {
		language = new Text();
		page = new Text();
		id = new IntWritable();
	}

	public Text getLanguage() {
		return language;
	}

	public void setLanguage(Text language) {
		this.language = language;
	}

	public Text getPage() {
		return page;
	}

	public void setPage(Text page) {
		this.page = page;
	}

	public IntWritable getId() {
		return id;
	}

	public void setId(IntWritable id) {
		this.id = id;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		language.write(out);
		page.write(out);
		id.write(out);
	}

	/**
	 * Compares this composite to another composite. Each of the fields are
	 * compared, in the order of id, language code, and page name. Each of
	 * the compared fields are compared based on the natural ordering of the
	 * data.
	 *
	 * @param o
	 *            The other composite to be compared to.
	 * @return A positive integer if this composite if comparatively before
	 *         the other, a negative integer if this composite if
	 *         comparatively after the other, and zero if both composites
	 *         are comparatively the same to each other.
	 */
	@Override
	public int compareTo(LanguagePageIdComposite o) {
		int compId = id.compareTo(o.id);
		if (compId != 0) {
			return compId;
		}
		int compLang = language.compareTo(o.language);
		if (compLang != 0) {
			return compLang;
		}
		int compPage = page.compareTo(o.page);
		return compPage;
	}

	/**
	 * Returns a hashcode of this composite based on language code, page
	 * name, and id.
	 */
	@Override
	public int hashCode() {
		return language.hashCode() * 331 + page.hashCode() * 163 + id.hashCode();
	}

	/**
	 * Determines if this composite is equal to another. Equal is defined as
	 * having the same text representation of language code and page name,
	 * and having the same integer representation of the id.
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof LanguagePageIdComposite) {
			LanguagePageIdComposite lpc = (LanguagePageIdComposite) o;
			return language.equals(lpc.language) && page.equals(lpc.page) && id.equals(lpc.id);
		}
		return false;
	}

	@Override
	public String toString() {
		return language + "\t" + page + "\t" + id;
	}

}
