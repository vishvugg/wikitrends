package org.amibee.split;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class SplitTopLngsPartitioner extends Partitioner<LanguagePageIdComposite, NullWritable> {

	@Override
	public int getPartition(LanguagePageIdComposite key, NullWritable value, int numPartitions) {
		return Math.abs(key.getLanguage().hashCode() % numPartitions);
	}
}
