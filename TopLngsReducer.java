package org.amibee.split;

import java.io.IOException;
import java.util.TreeMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * -Step 2.4-
 * 
 * This reducer finds the M popular languages. For each languages it aggregates
 * over the partial unique page counts received from its mappers and stores them
 * in a map. After all languages are processed it writes out the top M languages
 * and their unique page counts.
 *
 */
public class TopLngsReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

	private Long M_LANGUAGES = 0L;
	TreeMap<Long, String> languagesPageCountMap = new TreeMap<Long, String>();
	
	/**
	 * The setup method runs at the beginning of the reduce stage. It retrieves
     * the parameter M and stores it.
	 */
	@Override
	public void setup(Context context) throws IOException, InterruptedException {
		M_LANGUAGES = context.getConfiguration().getLong(WikiStats.NUM_LANGUAGES, WikiStats.DEFAULT_NUM_LANGUAGES);
	}

	/**
	 * The reducer receives language and list of partial unique page counts as
     * the inputs. It iterates over the list, aggregates them and stores them in
     * a map with language as value and its unique page count as the key.
	 */
	@Override
	protected void reduce(Text language, java.lang.Iterable<LongWritable> pages, Context Context) throws java.io.IOException, InterruptedException {
		long temp = 0;
		for (LongWritable PageCountsFromEachSplit : pages) {
			temp += PageCountsFromEachSplit.get();
		}
		languagesPageCountMap.put(temp, language.toString());
	}

	/**
	 * Clean up method is called when all records are processed. Here the M
     * largest keys (unique page counts) and their corresponding language codes
     * are retrieved and outputted.
	 */
	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		super.cleanup(context);
		for (int i = 0; i < M_LANGUAGES; i++) {
			if (!languagesPageCountMap.isEmpty()) {
				Long temp = languagesPageCountMap.lastKey();
				String tempLang = languagesPageCountMap.get(temp);
				context.write(new Text(tempLang), new LongWritable(temp));
				languagesPageCountMap.remove(temp);
			}
		}
	}
}
