package org.amibee.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

/**
 * -Step 4.2-
 * 
 * The Combiner associated with the top pages job of WikiStats. This Combiner
 * extends the generic Reducer for the top pages job. This Combiner uses the
 * cleanup() method in order to cycle through all the top language codes, and
 * cycles through all the top page spikes for the language, and writing out a
 * composite with language code and page name mapped to the page spike.
 *
 */
public class TopPgsCombiner extends TopPgsGenReducer<LanguagePageIdComposite, LongWritable> {

	/**
	 * Override the cleanup method in order to write out the data in the
	 * Reducer. This will be called once all records have been reduced. This
	 * method cycles through all the top language codes, and cycles through all
	 * the top page spikes for the language, and writing out a composite with
	 * language code and page name mapped to the page spike.
	 *
	 * @param context
	 *            The context associated with this job, can be used to access
	 *            the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {

		// Cycle through all the language codes
		for (String languageCode : highestSpikePages.keySet()) {

			// Cycle through all page spikes for the language code
			TreeMap<Long, ArrayList<String>> topPageSpikes = highestSpikePages.get(languageCode);
			for (Long pageSpike : topPageSpikes.descendingKeySet()) {

				ArrayList<String> pageNames = topPageSpikes.get(pageSpike);
				for (String pageName : pageNames) {
					context.write(new LanguagePageIdComposite(new Text(languageCode), new Text(pageName), new IntWritable(1)), new LongWritable(pageSpike));
				}
			}
		}
	}
}
