package org.amibee.split;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * -Step 2.3-
 * 
 * This is the mapper for "Top Languages from all splits" job. It receives
 * records from the intermediate results output by the reducers in Top languages
 * per split job. The entries from the intermediate files are of the format
 * language partial-unique-page-count. Mapper outputs
 *    <Language, partial-unique-page-count> as the <key, value> pair
 *
 */
public class TopLngsMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		//split the record on spaces
		String[] temp = value.toString().split("\\s+");
		//first token is language
		Text language = new Text(temp[0]);
		//second token is partial unique page count
		LongWritable count = new LongWritable(Long.parseLong(temp[1]));
		context.write(language, count);
	}
}
