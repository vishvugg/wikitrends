package org.amibee.split;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * -Step 4.1-
 * 
 * The Mapper used for the top pages job of Wikistats. The Mapper takes in
 * the output of the all pages Reducer. This input is text, with each line
 * containing a language code, page name, and spike for the associated
 * "O day" period. The output for this Mapper is a key composite of the
 * language code and page name. The value of the output is a LongWritable
 * with the spike for that particular page.
 *
 */
public class TopPgsMapper extends Mapper<LongWritable, Text, LanguagePageIdComposite, LongWritable> {

	/**
	 * The map method for this Mapper. This method takes in a line of text
	 * from the output of the all pages reducer. The key is the line offset
	 * in the file, and the value is the line of text. The line of text
	 * should contain a language code, a page name, and a spike.
	 *
	 * @param key
	 *            The key for the line of text. By default, this key is the
	 *            line offset in the file for the value.
	 * @param value
	 *            The line of text in a WikiStats file. Formatted as
	 *            <languageCode> <pageName> <pageSpike>
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();

		// Match the input text line formatted as: ln pageName spike
		Pattern pattern = Pattern.compile("^([a-z]{2}) (\\S+) (\\d+)");
		Matcher matcher = pattern.matcher(line);

		// Set the id associated with this page. The id will be set
		// arbitrarily to 1 since this is irrelevant in this stage.
		int id = 1;

		if (matcher.matches()) {
			// Setup composite key with the language code, page name, and id
			LanguagePageIdComposite lpc = new LanguagePageIdComposite(new Text(matcher.group(1)), new Text(matcher.group(2)), new IntWritable(id));
			// Write out the composite key mapped to the page spike
			context.write(lpc, new LongWritable(Long.parseLong(matcher.group(3))));
		}
	}
}
