package org.amibee.split;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * The Partitioner to be used between the Mapper and Reducer for the all
 * pages job of WikiStats. This Partitioner will be used in order to ensure
 * that all data with the same id is sent to the same Reducer, which is
 * essential. The id is simply modded by the number of partitions. This may
 * not produce even partitions; however, based on the WikiStats data, this
 * should be sufficiently even since the data is fairly evenly distributed.
 *
 * @author Team 5
 */
public class AllPgsPartitioner extends Partitioner<LanguagePageIdComposite, LongWritable> {

	/**
	 * Finds the partition that a key-value pair should be a part of.
	 *
	 * @param key
	 *            The composite key from the all pages Mapper.
	 * @param value
	 *            The page views value from the all pages Mapper.
	 * @param numPartitions
	 *            The number of partitions being used for this job.
	 * @return The id of the composite key modded by the number of
	 *         partitions.
	 */
	@Override
	public int getPartition(LanguagePageIdComposite key, LongWritable value, int numPartitions) {
		// partition = id (mod numPartitions)
		return Math.abs(key.getId().get() % numPartitions);
	}
}
