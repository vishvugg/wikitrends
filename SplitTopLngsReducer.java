package org.amibee.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * -Step 2.2-
 * 
 * This reducer receives (language, page, id=1) as the input key and null as the
 * input value. It then stores each language in a hashmap with language as the
 * key and the no of unique pages seen in that language so far as the value. The
 * final unique counts for each language seen so far is written out to the
 * context for the next mapper.
 *
 */
public class SplitTopLngsReducer extends Reducer<LanguagePageIdComposite, NullWritable, Text, LongWritable> {
	HashMap<String, Long> languagesPageCountMap = new HashMap<String, Long>();

	// A running total of languages seen
	Long running = 0L;

	// Cache of the last language code
	String lastLanguageCode = "";

	@Override
	protected void reduce(LanguagePageIdComposite lpc, java.lang.Iterable<NullWritable> pages, Context Context) throws java.io.IOException, InterruptedException {
		// if the current language is same as the previous language increment its count
		if (lastLanguageCode.equals(lpc.getLanguage().toString())) {
			running++;
		} else {
			// This language is different then the last, and since it is sorted,
			// the last one can be stored with the running count
			if (running != 0) {
				languagesPageCountMap.put(lastLanguageCode, running);
			}
			// Reset the running count and cache the language code
			running = 1L;
			lastLanguageCode = lpc.getLanguage().toString();
		}
	}

	/**
	 * The cleanup for this Reducer consists of writing out all the language
	 * codes and unique pages found in the reduce method. Before output, the
	 * reduce method is called one last time to flush out the last language code
	 * and page count.
	 */
	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		super.cleanup(context);
		// Flush the current language code
		reduce(new LanguagePageIdComposite(), new ArrayList<NullWritable>(), context);
		for (String language : languagesPageCountMap.keySet()) {
			Long languagePageCount = languagesPageCountMap.get(language);
			context.write(new Text(language), new LongWritable(languagePageCount));
		}
	}
}
