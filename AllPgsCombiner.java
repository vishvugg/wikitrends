package org.amibee.split;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;

/**
 * -Step 3.2-
 * 
 * A Combiner associated with the all pages job of WikiStats. This combiner
 * extends the generic reducer for the all pages job. This Combiner uses the
 * write to context to write out the given LanguagePageIdComposite and the
 * associated page views spike.
 *
 */
public class AllPgsCombiner extends AllPgsGenReducer<LanguagePageIdComposite, LongWritable> {

	/**
	 * Uses the context to write out the LanguagePageIdComposite as a key
	 * mapped to the page views spike value.
	 */
	@Override
	protected void writeToContext(Context context, LanguagePageIdComposite lpic, long spike) throws IOException, InterruptedException {
		// Write out the composite mapped to the page views spike
		context.write(lpic, new LongWritable(spike));
	}

}
