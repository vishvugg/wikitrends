package org.amibee.split;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 * -Step 3.1-
 * 
 * The Mapper used for the pass over all the WikiStats data. The Mapper will
 * take input from WikiStats files, the output key is a composite with the
 * language code, page name, and id. The id for output is the number of the
 * corresponding "O day" section. The value of the output is a LongWritable
 * with the page views for that particular page. This Mapper has a setup
 * phase for making a list with the most popular language codes. The only
 * composite keys that are output consist of one of the most popular
 * language codes.
 *
 */
public class AllPgsMapper extends Mapper<LongWritable, Text, LanguagePageIdComposite, LongWritable> {

	/**
	 * A list for storing the language codes of the most popular languages.
	 */
	ArrayList<String> mostPopularLanguages = new ArrayList<String>();

	// A cache for storing the most recently used language codes.
	// By nature, the Mapper will be receiving the same code repeatedly
	// and this is more efficient than looping the list every time.
	String lastSuccessfulLanguageCode = "";
	String lastUnsuccessfulLanguageCode = "";

	// Store the last file name and the ids to use for that filename
	// These are cached so they don't have to be calculated every time
	// The list of ids will be changed if a new file name is encountered
	String lastFileName = "";
	List<Integer> cachedIds = new ArrayList<Integer>();

	// Store the "O day" period and max number of days for this mapper
	Long ODayPeriod = 5L;
	Long maxDays = 60L;

	/**
	 * -Step 3.1.1-
	 * 
	 * Setup phase for the this Mapper. The file for most popular language
	 * codes and unique pages is retrieved through the file system. The file
	 * is read in, and each of the language codes in the file is read in and
	 * stored in the list.
	 *
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	public void setup(Context context) throws IOException, InterruptedException {

		// Set the "O day" period and the max number of days
		ODayPeriod = context.getConfiguration().getLong(WikiStats.NUM_DAYS, WikiStats.DEFAULT_NUM_DAYS);
		maxDays = context.getConfiguration().getLong(WikiStats.MAX_DAYS, WikiStats.DEFAULT_MAX_DAYS);

		// Obtain a path to the file that lists the most popular languages
		Path mostPopularLanguagesPath = new Path(WikiStats.MOST_POPULAR_LANGUAGES_FILE);
		FileSystem fs = FileSystem.get(context.getConfiguration());
		FileStatus[] fileStatuses = fs.listStatus(mostPopularLanguagesPath);
		for (FileStatus file : fileStatuses) {

			if (!file.isDir()) {
				// Setup a buffered reader for the most popular languages file
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(file.getPath())));

				// Read through the file, and store each language code in the
				// list
				String languageCodePages;
				while ((languageCodePages = br.readLine()) != null) {

					// The file lines must match the pattern of <languageCode>
					// <uniquePages>
					Pattern pattern = Pattern.compile("^([a-z]{2})\\s+(\\d+)");
					Matcher matcher = pattern.matcher(languageCodePages);
					if (matcher.matches()) {
						mostPopularLanguages.add(matcher.group(1));
					}
				}
			}
		}
	}

    /**
	 * Only for 2013... and through March.
	 */
	private static final int DAYS_OF_MONTH[] = { 31, 28, 31 };

    /**
     * Creates a list of IDs to use for the current file.
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
     * @return The list of IDs the current filename belongs to.
     */
	public List<Integer> getIDsFromFilename(Context context) {
		// the O day period ids the data is contained in
		List<Integer> ids = new ArrayList<Integer>();

		// the day number of the file
		int days = 0;

        // get the file name
        FileSplit fileSplit = (FileSplit)context.getInputSplit();
        String filename = fileSplit.getPath().getName();

		// parse the file name
		Pattern filePattern = Pattern
				.compile("pagecounts-(\\d{4})(\\d{2})(\\d{2})-\\d{6}(?:.gz)");
    	Matcher fileMatcher = filePattern.matcher(filename);

    	if (fileMatcher.matches()) {
            // get the captured information
    		int month = Integer.valueOf(fileMatcher.group(2));
    		int day = Integer.valueOf(fileMatcher.group(3));

            // add the days found
			days += day;

            // then add the number of days in each month after
			for (int i = 0; i < month - 1; i++) {
				days += DAYS_OF_MONTH[i];
			}

            // now fill the list with the IDs
			for (int i = 0; i < ODayPeriod; i++) {
				int id = days - i;
				if (id > 0 && id + ODayPeriod - 1 <= maxDays) {
					ids.add(id);
				}
			}
		}
		return ids;
    }

	/**
	 * -Step 3.1.2-
	 * 
	 * The map function for the this Mapper. Given a WikiStats line as a
	 * Text value, the language code, page name, and page views are parsed.
	 * An id is assigned based on the corresponding "O day" section.
	 *
	 * @param key
	 *            The key for the line of text. By default, this key is the
	 *            line offset in the file for the value.
	 * @param value
	 *            The line of text in a WikiStats file. Formatted as
	 *            <languageCode> <pageName> <pageViews> <pageBytes>
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();

		// Match the input text line formatted as:
		// <languageCode> <pageName> <pageViews> <pageBytes>
		// Note: Only two letter language codes are considered
		Pattern pattern = Pattern.compile("^([a-z]{2}) (\\S+) (\\d+) \\d+");
		Matcher matcher = pattern.matcher(line);

		if (matcher.matches()) {

			// Retrieve the language code, attempt to avoid iterating over the list
			String languageCode = matcher.group(1);
			if (lastSuccessfulLanguageCode.equals(languageCode)) {
				// This language code is known to be in the most popular
			} else if (lastUnsuccessfulLanguageCode.equals(languageCode)){
				// This language code is known to not be in the most popular
				return;
			} else if (mostPopularLanguages.contains(languageCode)) {
				// The language code was found in the list of the most popular, so it is cached
				lastSuccessfulLanguageCode = languageCode;
			} else {
				// The language code was not found, so it is cached
				lastUnsuccessfulLanguageCode = languageCode;
				return;
			}

			// Determine the ids associated with this line for the "O day" period
			// Use the cached ids if the file name is still the same
			FileSplit fileSplit = (FileSplit)context.getInputSplit();
	        String filename = fileSplit.getPath().getName();
	        if (!filename.equals(lastFileName)) {
	        	cachedIds = getIDsFromFilename(context);
	        	lastFileName = filename;
	        }

            for (Integer id: cachedIds) {
			    // Setup composite key with the language code, page name, and id
			    LanguagePageIdComposite lpc = new LanguagePageIdComposite(new Text(matcher.group(1)), new Text(matcher.group(2)), new IntWritable(id));
			    // Write out the composite key mapped to the page views
			    context.write(lpc, new LongWritable(Long.parseLong(matcher.group(3))));
            }
		}
	}
}
