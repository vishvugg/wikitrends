package org.amibee.split;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;

/**
 * The Reducer associated with the all pages job of WikiStats. The reducer
 * extends the generic reducer for the all pages job. This reducer uses the
 * write to context to write out the given LanguagePageIdComposite, by
 * making a Text of the language code, page name, and spike, and maps that
 * to null.
 *
 */
public class AllPgsReducer extends AllPgsGenReducer<Text, NullWritable> {

	@Override
	protected void writeToContext(Context context, LanguagePageIdComposite lpic, long spike) throws IOException, InterruptedException {
		// Write out the text formatted as: languageCode pageName spike.
		// The text key is mapped to a null value
		context.write(new Text(lpic.getLanguage() + " " + lpic.getPage() + " " + spike), NullWritable.get());
	}

}
