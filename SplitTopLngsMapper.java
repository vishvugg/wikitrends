package org.amibee.split;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * -Step 2.1-
 * 
 * This mapper reads from the input files and writes out (Language, Page, ID=1)
 * as the output key and Null as the output value. The class reuses
 * LanguagePageIdComposite class using a constant value for the ID which is
 * irrelevant in this case.
 *
 */
public class SplitTopLngsMapper extends Mapper<LongWritable, Text, LanguagePageIdComposite, NullWritable> {

	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();

		// Match the input text line formatted as:
		// <languageCode> <pageName> <pageViews> <pageBytes>
		// Note: Only two letter language codes are considered
		Pattern pattern = Pattern.compile("^([a-zA-Z]{2}) (\\S+) (\\d+) \\d+");
		Matcher matcher = pattern.matcher(line);
		// some same number for the id
		// change languagePageId constructor to take two args and set the third
		// to a default number
		if (matcher.matches()) {
			LanguagePageIdComposite lpc = new LanguagePageIdComposite(new Text(matcher.group(1)), new Text(matcher.group(2)), new IntWritable(1));
			// Write out the composite key mapped to null
			context.write(lpc, NullWritable.get());
		}

	}

}
