/*
 * -Problem being solved-
 *
 * The problem addressed by this program is to identify the "N" Wikipedia pages
 * that have experienced the largest activity increases (spikes) across any
 * "O" day period for each of the "M" most common languages, where M, N and O
 * are provided by the input data.
 *
 * As an example, if input values were given as M=5, N=10 and O=10, the program
 * would output the ten Wikipedia pages that experienced the largest spike over
 * any ten day period in the dataset for each of the five most common languages.
 * The most common languages are those that have the largest number of unique pages
 * over the entire dataset. Pages deleted during the time frame are still counted
 * into this total.
 *
 * -Solution outline-
 *
 *	Steps numbers are listed in the code as -step x.y- where they appear.
 *
 * 1 Setup Configuration
 *   Read in command-line arguments. The arguments specify the input and output
 *   files. The arguments also specify the O day period to consider, the top M
 *   languages to be considered, and the N top spikes to consider for each of
 *   the M languages.
 *
 * 2 Determine the top M languages.
 *   Determine the M languages that have the most unique pages over the input data.
 *
 *   2.1 Mapping each language code.
 *       Read in each line of each input file. For each line, read the language
 *       code, ignoring codes that aren't two letters. Map each line as a composite of 
 *       language code and page name, mapped to null. 
 *
 *   2.2 Reducing the language code occurrences.
 *       Receive each of the composites from 2.1. The important idea here is that these
 *       composites are sorted, first by language code, then by page name. For each page,
 *       read it in, if the language hasn't been seen, start a counter, if the language
 *       has been seen, increment the counter. When a new language is encountered, store the
 *       previous one in a mapping to the number of unique pages. On cleanup, flush the last language
 *       code, and then output each language code to the number of unique pages found.
 *       
 *   2.3 Mapping each page count.
 *   	 Read in each language code and page count from 2.3. Map out the language code
 *   	 mapped to the number of pages.
 *   
 *   2.4 Reducing the page counts.
 *   	 On setup, determine the number of top languages needed. For each language code, 
 *   	 a list of page counts is received. This list of page counts is summed in order to
 *   	 get the total number of unique pages. This language code is stored in a map along with
 *   	 the number of unique pages. On cleanup, the top M languages are retrieved from the 
 *   	 map and written out.
 *
 * 3 Determine highest page spikes for each O day period.
 *	 
 *   3.1 Mapping each page to page views.
 *
 *       3.1.1 Setup list of top languages.
 *       	   Read in file for most popular language codes, read over file and store each
 *   	 	   code in a list of language codes. Match pattern of lines in the file with
 *   	 	   the pattern of <languageCode> <uniquePages>. If pattern is a match, add to
 *   	 	   list of most popular languages.
 *
 *       3.1.2 Map pages for popular languages.
 *             Match input to list of most popular languages. If found in the
 *             list, cache as a successful find. If not found in list, cache as
 *             an unsuccessful find. This makes for more efficiency
 *             rather than looping every time.
 *       	   Write out page composite if it is in one of the top languages.
 *
 *   3.2 Combining the page views.
 *       Cycle through all page views for each corresponding language code
 *       and page name. A spike is the difference between the lowest and highest
 *       page views for that page. Output the language code and page name
 *       mapped to the page spike.
 *
 *   3.3 Reducing the page views.
 *   	 Cycle through all page views for each corresponding language code
 *       and page name. A spike is the difference between the lowest and highest
 *       page views for that page. Output the language code and page name
 *       and spike to be read in for the next phase.
 *
 * 4 Determine highest page spikes for all periods.
 *
 *   4.1 Mapping each page to the O day spike.
 *       Use output from reducer of all pages as input for Mapper of top pages.
 *       Input consists of language code, page name and spike for a specific
 *       "O day" period. Match input text to formatting of language, page name,
 *       spike, and write composite keys mapped to page spike.
 *
 *   4.2 Combining the page spikes.
 *   	 See 4.3. The difference is that this will write out only page spikes, 
 *   	 not unique language pages.
 *
 *   4.3 Reducing the page spikes.
 *       Iterate over list of page spikes for a given page to find the highest.
 *       Then loop through page spikes again to determine next highest and so
 *       on. If there are fewer than N pages, insert the page and spike in the
 *       list of top spikes for that page.
 *
 *      4.3.1 Setup list of top languages and number of unique pages.
 *            Create map of top languages along with their number of
 *            unique pages. Populate this map by mapping each language code to
 *            the unique pages of that code.
 *
 *      4.3.2 Map language code to set of top page spikes.
 *            Create hash map of pages with highest spikes for each language.
 *            Populate this map by scanning over all spikes of that language to
 *            find the highest. Then run over all spikes to find next highest.
 *            If there are fewer than N pages, insert these pages and spikes.
 *
 *      4.3.3 Output top M languages, and each corresponding N page.
 *            Cycle through all language codes, then cycle through all page
 *            spikes for that language, writing out the language code and page
 *            name in descending order of page spike. Then write out the language 
 *            code and number of unique pages.
 *
 * -How to run the program-
 *
 *	Configuration

	To configure the tool, open "wikistats.pbs".
	Here, modify the number of nodes to use for this run. In addition, specify the max amount of time (wall time) to run for.
	Modify the below lines in "wikistats.pbs"
	#PBS -l nodes=NO_OF_NODES:ppn=8,pmem=2gb
	#PBS -l walltime=HH:MM:SS

	Next, edit the command line arguments section (lines 33-37). The arguments are:
	ARGSLANGS  	:	The number of popular languages to analyze the spikes for.
	ARGSSPIKES 	: 	The number of top page spikes to view for each of the popular languages.
	ARGSODAYS  	:	The O day period to use for this run. This will be every O day period over the input data. 
	ARGSNODES  	:	The number of nodes to use for this run. This is required as an argument in order to calculate the ideal number of reducers to use.
	ARGSMAXDAYS	:	The number of days to consider data over.

	Next, go to LOCALINPUT in line 50. Set this to the directory where the data is stored for input.
	Ideally, this directory will contain the same number of days as set in the max days args.
	Any additional files in the directory will be read over, but the data within will not be considered.

	Finally, go to LOCALOUTPUT in line 54. Set this to the directory where the output should be stored.
	The output will come in one file.

	For a collection of Wikistats data, see: /lustre/cs562178/wikistats
	The data in this directory is not guaranteed to be accurate or persistent.

	Execution

	To run the program, execute:
	chmod u+x runit.sh
	./runit.sh

	check progress using: qstat -U $USER

 * -Sample output-
 *
	The output for this tool will come in one file, with the following format:
	ln1 pageName1 pageSpike1
	...
	ln1 pageNameN pageSpikeN
	Language: ln1 uniquePages
	...
	lnM pageName1 pageSpike1
	...
	lnM pageNameN pageSpikeN
	Language: lnM uniquePages

	The pages with the highest spikes for a language are output first, followed by that language and the unique pages for the dataset.
	This is repeated for each of desired top languages.
	
	Sample output1:

	en		Main_Page	416922
	en		Main_Page	408376
	en		Main_Page	406799
	en		Main_Page	403604
	en		Main_Page	402964
	en		Main_Page	402073
	en		Main_Page	392830
	en		Main_Page	389642
	en		Main_Page	388404
	Language: en		24929013
	 
	Sample output2:
	
	en Main_Page	320359
	en Main_Page	286837
	en Special:Search	82324
	en Special:Random	78077
	en Special:Search	66666
	en Special:Random	61064
	en Dick_Clark	52143
	en Yakuza	44419
	en Jenny_McCarthy	43659
	en Petula_Clark	43233
	Language: en	31640207
	fr Pont_suspendu	23808
	fr Jacques_Martin_(animateur)	21760
	fr Poppy_Montgomery	19367
	fr Unforgettable	15537
	fr Sheila	15191
	fr Pont_suspendu	14727
	fr Sleepy_Hollow_(film)	12470
	fr Uranium	12296
	fr Windex.php	11291
	fr Le_Corniaud	10736
	Language: fr	5387861
 */

package org.amibee.split;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * This is the main class responsible for running the jobs and managing
 * inter-job I/O.
 */
public class WikiStats extends Configured implements Tool {

	/**
	 * The path of the directory to be used to store files that are transferred
	 * between the all pages and top pages job.
	 */
	public static final String JOB_INTERMEDIATE_FILE = "jobIntermediateFile";

	/**
	 * The path of the file to be used to store the language codes for the most
	 * popular languages after being determined.
	 */
	public static final String MOST_POPULAR_LANGUAGES_FILE = "mostPopularLanguagesFile";

	/**
	 * A string used to store the input representing the desired number of top
	 * languages to have pages listed for.
	 */
	public static final String NUM_LANGUAGES = "num_languages";

	/**
	 * A string used to store the input representing the desired number of top
	 * page spikes for each of the top languages.
	 */
	public static final String NUM_SPIKES = "num_spikes";

	/**
	 * A string used to store the input representing the desired number of days
	 * to split the input data into. This is regarded as the "O day" period.
	 */
	public static final String NUM_DAYS = "num_days";

	/**
	 * A string used to store the input representing the desired number of nodes
	 */
	public static final String NUM_NODES = "num_nodes";

	/**
	 * A string used to store the input representing the desired max number of
	 * days worth of data for input.
	 */
	public static final String MAX_DAYS = "max_days";

	/**
	 * The default number of top languages to have pages listed for.
	 */
	public static final long DEFAULT_NUM_LANGUAGES = 5;

	/**
	 * The default number of pages to have spikes listed for each of the top
	 * languages.
	 */
	public static final long DEFAULT_NUM_SPIKES = 10;

	/**
	 * The default "O day" period to split the input data into.
	 */
	public static final long DEFAULT_NUM_DAYS = 5;

	/**
	 * The default max number of days to have data for.
	 */
	public static final long DEFAULT_MAX_DAYS = 60;

	/**
	 * The default number of nodes to use.
	 */
	public static final long DEFAULT_NUM_NODES = 16;

	/**
	 * A file name used to transfer data between the two jobs for finding top
	 * languages.
	 */
	public static final String LANGUAGE_UNIQUE_PAGECOUNTS_PER_SPLIT_FILE = "languagesUniquePagecountsPerSplitFile";

	/**
	 * -Step 1.0-
	 * 
	 * The entry point for running a WikiStats Tool. Reads in the arguments and
	 * assures that they are present and formatted correctly. Sets up and starts
	 * the jobs, with a temporary file used to exchange data.
	 * 
	 * @param args
	 *            Args: Arguments for WikiStats formatted as: <in> <out>
	 *            <languages> <spikes> <Odays> <numNodes> (optional)<maxDays>
	 * @return Return 0 if successful
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted
	 * @throws ClassNotFoundException
	 *             Thrown by a running job
	 */
	@Override
	public int run(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

		// Check to ensure all arguments are present
		if (args.length != 6 && args.length != 7) {
			System.err.println("Usage: WikiStats <in> <out> <languages> <spikes> <Odays> <numNodes> (optional)<maxDays>");
			System.exit(1);
		}

		// Parse the input variables out of the arguments
		long numLanguages;
		long numSpikes;
		long numDays;
		long maxDays;
		long numNodes;

		try {
			// Try to parse all input variables. Exceptions are thrown for
			// invalid or negative longs
			numLanguages = Long.parseLong(args[2]);
			if (numLanguages < 0)
				throw new NumberFormatException("Cannot have negative number of languages.");
			numSpikes = Long.parseLong(args[3]);
			if (numSpikes < 0)
				throw new NumberFormatException("Cannot have negative number of spikes.");
			numDays = Long.parseLong(args[4]);
			if (numDays < 0)
				throw new NumberFormatException("Cannot have negative Odays.");
			numNodes = Long.parseLong(args[5]);
			if (numNodes < 0)
				throw new NumberFormatException("Cannot have negative number of nodes.");
			if (args.length == 7) {
				maxDays = Long.parseLong(args[6]);
				if (maxDays < 0)
					throw new NumberFormatException("Cannot have negative maxDays.");
			} else {
				maxDays = 60;
			}
		} catch (NumberFormatException e) {
			System.err.println("Invalid WikiStats arguments.");
			throw e;
		}

		// Setup a configuration for each job
		// The configuration is used to store the input variables
		// Use context.getConfiguration().getLong(VARIABLE, DEFAULT) to retrieve
		Configuration conf = new Configuration();
		conf.setLong(NUM_LANGUAGES, numLanguages);
		conf.setLong(NUM_SPIKES, numSpikes);
		conf.setLong(NUM_DAYS, numDays);
		conf.setLong(MAX_DAYS, maxDays);
		conf.setLong(NUM_NODES, numNodes);

		int jobReducerTasks = ((Double) (0.95 * numNodes * Integer.parseInt(conf.get("mapred.tasktracker.reduce.tasks.maximum", "2")))).intValue();

		FileSystem fs = FileSystem.get(conf);

		// Ensure that the file used to store intermediate data is deleted
		fs.delete(new Path(LANGUAGE_UNIQUE_PAGECOUNTS_PER_SPLIT_FILE), true);
		// This job finds the total number of unique pages for each language in
		// that split
		Job topLngsPerSplitJob = new Job(conf);
		topLngsPerSplitJob.setJarByClass(WikiStats.class);
		topLngsPerSplitJob.setJobName("Top languages");
		topLngsPerSplitJob.setMapperClass(SplitTopLngsMapper.class);
		topLngsPerSplitJob.setReducerClass(SplitTopLngsReducer.class);
		FileInputFormat.addInputPath(topLngsPerSplitJob, new Path(args[0]));
		FileOutputFormat.setOutputPath(topLngsPerSplitJob, new Path(LANGUAGE_UNIQUE_PAGECOUNTS_PER_SPLIT_FILE));
		topLngsPerSplitJob.setMapOutputKeyClass(LanguagePageIdComposite.class);
		topLngsPerSplitJob.setMapOutputValueClass(NullWritable.class);
		topLngsPerSplitJob.setOutputKeyClass(Text.class);
		topLngsPerSplitJob.setOutputValueClass(LongWritable.class);
		topLngsPerSplitJob.waitForCompletion(true);

		// this job finds the popular languages
		Job topLngsFromAllSplits = new Job(conf);
		topLngsFromAllSplits.setJarByClass(WikiStats.class);
		topLngsFromAllSplits.setJobName("Top languages for all splits");
		topLngsFromAllSplits.setNumReduceTasks(1);

		topLngsFromAllSplits.setMapperClass(TopLngsMapper.class);
		topLngsFromAllSplits.setReducerClass(TopLngsReducer.class);
		topLngsFromAllSplits.setMapOutputKeyClass(Text.class);
		topLngsFromAllSplits.setMapOutputValueClass(LongWritable.class);

		FileInputFormat.addInputPath(topLngsFromAllSplits, new Path(LANGUAGE_UNIQUE_PAGECOUNTS_PER_SPLIT_FILE));
		// Ensure that the directory to store intermediate data doesn't exist
		fs.delete(new Path(MOST_POPULAR_LANGUAGES_FILE), true);
		FileOutputFormat.setOutputPath(topLngsFromAllSplits, new Path(MOST_POPULAR_LANGUAGES_FILE));
		topLngsFromAllSplits.waitForCompletion(true);

		// Setup the all pages job
		Job allPgsJob = new Job(conf);
		allPgsJob.setJarByClass(WikiStats.class);
		allPgsJob.setJobName("WikiStatsAllPgs");

		// Delete the temporary file if it already exists
		fs.delete(new Path(JOB_INTERMEDIATE_FILE), true);

		// Set the input to the desired directory and set the output to be the
		// temporary file
		FileInputFormat.addInputPath(allPgsJob, new Path(args[0]));
		FileOutputFormat.setOutputPath(allPgsJob, new Path(JOB_INTERMEDIATE_FILE));

		// Set the Mapper, Combiner, and Reducer classes
		allPgsJob.setMapperClass(AllPgsMapper.class);
		allPgsJob.setCombinerClass(AllPgsCombiner.class);
		allPgsJob.setReducerClass(AllPgsReducer.class);

		allPgsJob.setNumReduceTasks(jobReducerTasks);

		// Setup the output classes for the keys and values
		allPgsJob.setMapOutputKeyClass(LanguagePageIdComposite.class);
		allPgsJob.setMapOutputValueClass(LongWritable.class);
		allPgsJob.setOutputKeyClass(Text.class);
		allPgsJob.setOutputValueClass(NullWritable.class);

		// Start the first job. This does not return until the job completes.
		allPgsJob.waitForCompletion(true);

		// Setup the top pages job
		Job topPgsJob = new Job(conf);
		topPgsJob.setJarByClass(WikiStats.class);
		topPgsJob.setJobName("WikiStatsTopPgs");

		// Set the input to the temporary file and set the output to be the
		// desired directory
		FileInputFormat.addInputPath(topPgsJob, new Path(JOB_INTERMEDIATE_FILE));
		FileOutputFormat.setOutputPath(topPgsJob, new Path(args[1]));

		// Set the Mapper, Combiner, and Reducer classes
		topPgsJob.setMapperClass(TopPgsMapper.class);
		topPgsJob.setCombinerClass(TopPgsCombiner.class);
		topPgsJob.setReducerClass(TopPgsReducer.class);

		// The job must have only one reducer since all the data needs to
		// be handled at once
		topPgsJob.setNumReduceTasks(1);

		// Setup the output classes for the keys and values
		topPgsJob.setMapOutputKeyClass(LanguagePageIdComposite.class);
		topPgsJob.setMapOutputValueClass(LongWritable.class);
		topPgsJob.setOutputKeyClass(Text.class);
		topPgsJob.setOutputValueClass(LongWritable.class);

		// Start the job. This does not return until the job completes.
		topPgsJob.waitForCompletion(true);

		// Exit with 0 to signify that jobs completed successfully
		return 0;
	}

	/**
	 * Main access point for the WikiStats program. Uses ToolRunner to start an
	 * instance of WikiStats with the given args.
	 */
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new WikiStats(), args);
		System.exit(exitCode);
	}

}
