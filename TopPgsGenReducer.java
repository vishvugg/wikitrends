package org.amibee.split;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * A generic Reducer associated with the top pages job of WikiStats. The Reducer
 * receives input from the top pages Mapper, which is formatted as a composite
 * key and page spike as the value. This reducer finds the highest page spike
 * for an "O day" period for each page. In addition, this reducer disregards
 * pages that do not have one of the highest page spikes. A map is kept of each
 * language and the pages with the highest spikes for that language. If the page
 * spike is not one of the N highest, it is disregarded. The cleanup() method
 * must be overwritten in order to write out the content of this Reducer at the
 * end.
 *
 *
 * @param <U>
 *            The output key class of reducers extending this class.
 * @param <V>
 *            The output value class of reducers extending this class.
 */
public class TopPgsGenReducer<U, V> extends Reducer<LanguagePageIdComposite, LongWritable, U, V> {

	/**
	 * A mapping from each of the most popular language codes to the
	 * corresponding number of unique pages.
	 */
	TreeMap<Long, String> mostPopularLanguages = new TreeMap<Long, String>();

	/**
	 * -Step 4.3.1-
	 * 
	 * Setup phase for the this Reducer. The file for most popular language
	 * codes and unique pages is retrieved through the file system. The file is
	 * read in, and each of the language codes in the file is mapped to the
	 * corresponding number of unique pages.
	 *
	 * @param context
	 *            The context associated with this job, can be used to access
	 *            the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.@param context
	 */
	@Override
	public void setup(Context context) throws IOException, InterruptedException {

		tpsMaxSize = context.getConfiguration().getLong(WikiStats.NUM_SPIKES, WikiStats.DEFAULT_NUM_SPIKES);

		// Obtain a path to the file that holds the most popular languages
		Path mostPopularLanguagesPath = new Path(WikiStats.MOST_POPULAR_LANGUAGES_FILE);
		FileSystem fs = FileSystem.get(context.getConfiguration());
		FileStatus[] fileStatuses = fs.listStatus(mostPopularLanguagesPath);
		for (FileStatus file : fileStatuses) {
			if (!file.isDir()) {
				// Setup a buffered reader for the most popular languages file
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(file.getPath())));

				// Read through the file, and store each language code in the
				// list
				String languageCodePages;
				while ((languageCodePages = br.readLine()) != null) {
					// The file lines must match the pattern of <languageCode>
					// <uniquePages>
					Pattern pattern = Pattern.compile("^([a-z]{2})\\s+(\\d+)");
					Matcher matcher = pattern.matcher(languageCodePages);
					if (matcher.matches()) {
						mostPopularLanguages.put(Long.parseLong(matcher.group(2)), matcher.group(1));
					}
				}
			}
		}
	}

	/**
	 * -Step 4.3.2-
	 * 
	 * A data structure for holding the pages with the highest page spikes for
	 * each language. Contains a hashing from a language code to a tree map.
	 * Each tree map maps a spike to the page name associated with that spike.
	 */
	HashMap<String, TreeMap<Long, ArrayList<String>>> highestSpikePages = new HashMap<String, TreeMap<Long, ArrayList<String>>>();

	Long tpsMaxSize = 0L;

	@Override
	public void reduce(LanguagePageIdComposite lpic, Iterable<LongWritable> spikes, Context context) throws IOException, InterruptedException {

		// Begin with the highest spike number to be 0
		long highestSpike = 0;

		// Cycle through all the page spikes to find the highest.
		for (LongWritable spike : spikes) {
			if (spike.get() > highestSpike) {
				highestSpike = spike.get();
			}
		}

		insertIntoHighestSpikePages(lpic, highestSpike, context);
	}

	/**
	 * Checks to see if the given page spike is within the designated highest
	 * number of spikes for the associated language code. The desired number of
	 * highest pages is retrieved through the job configuration. If the spike is
	 * in the highest, the current lowest for that language is removed and the
	 * new spike is inserted.
	 * 
	 * @param lpic
	 *            The composite for the current page.
	 * @param highestSpike
	 *            The highest spike associated with the page.
	 * @param context
	 *            The context associated with this job, can be used to access
	 *            the Configuration.
	 * @return True is the new page spike is inserted into the map, false
	 *         otherwise.
	 */
	private boolean insertIntoHighestSpikePages(LanguagePageIdComposite lpic, long highestSpike, Context context) {

		// Retrieve the language code and page name from the composite
		String languageCode = lpic.getLanguage().toString();
		String pageName = lpic.getPage().toString();

		// Check to see if this language already has a mapping
		if (highestSpikePages.get(languageCode) == null) {

			// Map the language code to a new tree map
			highestSpikePages.put(languageCode, new TreeMap<Long, ArrayList<String>>());
		}

		// If there are fewer than N pages, insert the page and spike
		TreeMap<Long, ArrayList<String>> topPageSpikes = highestSpikePages.get(languageCode);

		int tpsSize = 0;
		for (Long key : topPageSpikes.keySet()) {
			tpsSize += topPageSpikes.get(key).size();
		}
		if (tpsSize < context.getConfiguration().getLong(WikiStats.NUM_SPIKES, WikiStats.DEFAULT_NUM_SPIKES)) {
			if (topPageSpikes.get(highestSpike) == null) {
				topPageSpikes.put(highestSpike, new ArrayList<String>());
			}
			topPageSpikes.get(highestSpike).add(pageName);
			return true;
		}

		// Check to see if the highest spike for this page is higher than
		// the lowest of the top spikes, if it is, remove the lowest, and
		// insert the new spike and page
		long lowestSpike = topPageSpikes.firstKey();
		if (highestSpike > lowestSpike) {
			if (topPageSpikes.get(lowestSpike).size() == 1) {
				topPageSpikes.remove(lowestSpike);
				if (topPageSpikes.get(highestSpike) == null) {
					topPageSpikes.put(highestSpike, new ArrayList<String>());
				}
				topPageSpikes.get(highestSpike).add(pageName);
				return true;
			} else {
				topPageSpikes.get(lowestSpike).remove(0);
				if (topPageSpikes.get(highestSpike) == null) {
					topPageSpikes.put(highestSpike, new ArrayList<String>());
				}
				topPageSpikes.get(highestSpike).add(pageName);
				return true;
			}
		}

		// The spike was not inserted into the mapping
		return false;
	}

}
