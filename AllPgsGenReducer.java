package org.amibee.split;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * -Step 3.3-
 * 
 * A generic reducer associated with the all pages job of WikiStats. This
 * reducer receives input from the all pages Mapper, which is formatted as a
 * composite key and page views as the value. The pages for input are
 * partitioned based on id. This reducer finds the page spike for each of
 * the given pages. A spike is the difference between lowest and highest
 * page views for a page. If there is only one page view listed, that is the
 * spike. The information is written to context based on the implementation
 * of the writeToContext() method.
 *
 *
 * @param <U>
 *            The output key class of reducers extending this class.
 * @param <V>
 *            The output value class of reducers extending this class.
 */
public abstract class AllPgsGenReducer<U, V> extends Reducer<LanguagePageIdComposite, LongWritable, U, V> {

	/**
	 * The reduce method for the this Reducer. The method is given the
	 * information about a page, and the list of page views for that page
	 * and id. The spike is determined from the list of page views.
	 *
	 * @param lpc
	 *            The composite key for the page.
	 * @param pageCounts
	 *            An Iterable of page views for the associated page.
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	@Override
	public void reduce(LanguagePageIdComposite lpc, Iterable<LongWritable> pageCounts, Context context) throws IOException, InterruptedException {

		// Set the min and max values to the extremes
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;

		// Iterate through the page views, and set the min and max to the
		// lowest and highest page views, respectively
		// Also track the number of page views
		int noOfOccurrences = 0;
		for (LongWritable pageCount : pageCounts) {
			long temp = pageCount.get();
			if (temp < min)
				min = temp;
			if (temp > max)
				max = temp;
			noOfOccurrences++;
		}

		// The spike is the difference between highest and lowest page views
		long spike = max - min;

		// If there was only one page view in the iteration, the page was
		// only listed once, so let the spike be the number of views
		spike = (noOfOccurrences == 1 ? max : spike);

		writeToContext(context, lpc, spike);
	}
	
	/**
	 * A method for writing to context based on the information composite
	 * and the page views spike. Context must be written to according to the
	 * classes defined as U and V.
	 * 
	 * @param context
	 *            The context associated with this job, can be used to
	 *            access the Configuration.
	 * @param lpic
	 *            The composite containing information for that page.
	 * @param spike
	 *            The spike in page views associated with the composite.
	 * @throws IOException
	 *             Thrown if communication with the JobTracker is lost.
	 * @throws InterruptedException
	 *             Thrown if a job is interrupted.
	 */
	protected abstract void writeToContext(Context context, LanguagePageIdComposite lpic, long spike) throws IOException, InterruptedException;
	
}
